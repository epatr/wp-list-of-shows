<div class="content has-text-centered">
    
    <?php get_search_form(); ?>

</div>

<div class="content is-hidden-touch">
    <h3 class="title">Table of Contents</h3>
    
    <ul class="menu-list"><?php wp_list_pages(); ?></ul>
</div>