<?php

if (!function_exists('list_of_shows_setup')) :
function list_of_shows_setup() {
        add_theme_support('post-thumbnails');
        add_theme_support('post-formats', array('aside', 'gallery', 'quote', 'image', 'video', 'audio'));
        add_theme_support('title-tag');
        
        register_nav_menus(array(
                'primary' => 'Primary Menu'
            ));
        
        // Add support for flyer thumbnails
        add_image_size('flyer-thumb', 240, 360, true);
        add_image_size('flyer-tiny', 75, 150);
    }
endif;
add_action('after_setup_theme', 'list_of_shows_setup');

// Add stuff to the head
function list_of_shows_scripts() {
        wp_enqueue_style('google-fonts-merriweather', '//fonts.googleapis.com/css?family=Merriweather:400,400i,700,700i|Oswald:300,400,700', array(), null);
        wp_enqueue_style('stylesheet', get_template_directory_uri() . '/style.css', array(), null);
}
add_action('wp_enqueue_scripts', 'list_of_shows_scripts');

// Control the read more string
function list_of_shows_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'list_of_shows_excerpt_more' );
