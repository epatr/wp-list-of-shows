<hr>
<div>
    <ol class="section">
        <?php
            wp_list_comments( array(
                'style'       => 'ol',
                'short_ping'  => true,
                'avatar_size' => 56,
            ) );
        ?>
    </ol><!-- .comment-list -->
    
    
    <div class="box"><?php comment_form(); ?></div>
</div>