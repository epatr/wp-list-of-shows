<?php

get_header();

?>

<?php 
$todaysDate = date('Ymd');
$event_query = array(
	'posts_per_page'	=> -1,
	'post_type'			=> 'event',
	'order' => 'ASC',
	'meta_key' => 'event_date',
	'orderby' => 'meta_value',
	'meta_query'	=> array(
		array(
			'key'		=> 'event_date',
			'value'		=> $todaysDate,
			'type'		=> 'NUMERIC',
			'compare'	=> '>='
		)
	)
);

$the_query = new WP_Query( $event_query );

if( $the_query->have_posts() ) :  ?>
	
	<?php while ( $the_query->have_posts() ) : $the_query->the_post();
	
    $eventDate = DateTime::createFromFormat('Ymd', $post->event_date)->format('l, F jS');
    
     ?>
     	<div class="media box">
			<div class="media-left">
			<a href="<?php the_permalink(); ?>">
			<?php if ($post->flyer) :
				$image = get_field('flyer');
				echo '<img src="' , $image['sizes']['flyer-tiny'] , '">';
			endif; ?></a>
			</div>
			<div class="media-content">
				<div class="content">
					<h3 class="title is-4"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				    <h5 class="subtitle is-6"><?php echo $eventDate; ?></h5>
					<p><?php the_excerpt(); ?></p>
				</div>
			</div>
		</div>
	<?php 
	
	endwhile;
	?>
<?php endif; ?>

<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>


<?php

get_footer();

?>