<?php 

$posts = get_field('venue');

if( $posts ): 

    foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>

    	<a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a>

	<?php endforeach; ?>

<?php endif; ?>