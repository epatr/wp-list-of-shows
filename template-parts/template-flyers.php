<?php 

/* Template Name: Flyers */

get_header();

?>

<h1><?php the_title(); ?></h1>
<p><?php 
$content = apply_filters( 'the_content', $post->post_content );
echo $content;
?></p>

<?php 
$todaysDate = date('Ymd');
$event_query = array(
	'posts_per_page'	=> -1,
	'post_type'			=> 'event',
	'order' => 'ASC',
	'meta_key' => 'event_date',
	'orderby' => 'meta_value',
	'meta_query'	=> array(
		array(
			'key'		=> 'event_date',
			'value'		=> $todaysDate,
			'type'		=> 'NUMERIC',
			'compare'	=> '>='
		)
	)
);

$the_query = new WP_Query( $event_query );

if( $the_query->have_posts() ): 
	
	// Set up a count to start a new row after 4 posts
	$count = 1;
	?>
	
	<?php while ( $the_query->have_posts() ) : $the_query->the_post();
	
	if ($count === 1) : echo '<div class="tile is-ancestor">'; endif;
    $eventDate = DateTime::createFromFormat('Ymd', $post->event_date)->format('F j');
    
     ?>
     	<div class="tile is-parent is-3">
			<div class="tile is-child box">
			<a href="<?php the_permalink(); ?>">
			<?php if ($post->flyer) :
				$image = get_field('flyer');
				echo '<img src="' , $image['sizes']['flyer-thumb'] , '">';
			endif; ?>
			</a>
			</div>
		</div>
	<?php 
	
	// Increment the count, and if it reaches 5 end the row and restart
	$count++;
	if ($count === 5) : echo '</div>'; $count = 1; endif;
	endwhile;
	
	if ($count !== 5) : echo '</div>'; endif;
	?>
<?php endif; ?>

<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>


<?php

get_footer();

?>

------------------------------------------------------------------------------------------------------------------------------------