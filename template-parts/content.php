<?php 

$articleView = is_single() || is_page(); 
$singleView = is_page();

?>

<div class="article">
    
    <?php
    if ($articleView) {
		the_title( '<h1 class="title is-1">', '</h1>' );
	} else {
		the_title( '<h2 class="title is-3"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
	}
	
	if (!$singleView) {
	?>
    <div class="article__metadata">Posted <span><?php echo get_the_date(); ?></span> by <?php the_author_posts_link(); ?> <?php if(get_the_category()): echo ' in ' , the_category(); endif; ?></div>
    <?php
	}
	
    if ($articleView) : ?>
    <div class="article__content">
        <?php the_content(); ?>
        
        <?php if ($post->post_type === 'event') : ?>
            
            <?php
            $eventDate = DateTime::createFromFormat('Ymd', $post->event_date)->format('l, F j');
            $eventVenueID = $post->venue[0];
            
            get_template_part('/template-parts/snippets/event_flyer');
            ?>
            
            <pre><?php var_dump($post->venue); ?></pre>
            
            <ul>
                <li>Date: <?= $eventDate ?></li>
                <li>Venue: <a href="<?= get_the_permalink($eventVenueID); ?>"><?= get_the_title($eventVenueID); ?></a> </li>
                <li>Time: <?= get_post_meta($post->ID, 'event_time', true); ?></li>
                <li>Cost: <?= get_post_meta($post->ID, 'cost', true); ?></li>
                <li>URL: <a href="<?= $post->outgoing_url ?>" target="_blank"><?= $post->outgoing_url ?></a></li>
            </ul>

        <?php endif; // post type = event ?>
    
    
    </div>
    
    <div class="content">
    <?php if ( comments_open() || get_comments_number() ) :
        comments_template();
    endif; ?>
    </div>
    <hr>
    <div class="content">
        <?php the_meta(); ?>
        <pre><?php var_dump($post); ?></pre>
    </div>
    
    
	<?php else : ?>
    <div class="article__summary"><?php the_excerpt(); ?></div>
    <?php endif; ?>
</div>