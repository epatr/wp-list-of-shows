<?php

get_header();

?>
<h1><?= the_title(); ?></h1>

<ul>
	<li>Address: <?php the_field('address'); ?></li>
	<li>Phone: <?php the_field('phone'); ?></li>
	<li>Website: <a href="<?php the_field('website'); ?>"><?php the_field('website'); ?></a></li>
	<li>Email: <a href="mailto: <?php the_field('email'); ?>"><?php the_field('email'); ?></a></li>
	<li>Facebook: <a href="<?php the_field('facebook'); ?>"><?php the_field('facebook'); ?></a></li>
	<li>Twitter: <a href="<?php the_field('twitter'); ?>"><?php the_field('twitter'); ?></a></li>
	<li>Instagram: <a href="<?php the_field('instagram'); ?>"><?php the_field('instagram'); ?></a></li>
</ul>

<p><?php 
$content = apply_filters('the_content', $post->post_content); 
echo $content;
?></p>
<hr>

<h5 class="is-5 title">Upcoming Shows</h5>
<ul>
<?php 
$todaysDate = date('Ymd');
$event_query = array(
	'posts_per_page'	=> -1,
	'post_type'			=> 'event',
	'order' => 'ASC',
	'meta_key' => 'event_date',
	'orderby' => 'event_date',
	'meta_query'	=> array(
	    'relation' => 'AND',
	    array(
	        'key' => 'venue',
	        'value' => $post->ID,
	        'type=' => 'NUMERIC',
	        'compare' => 'LIKE'
        ),
        array(
			'key'		=> 'event_date',
			'value'		=> $todaysDate,
			'type'		=> 'NUMERIC',
			'compare'	=> '>='
		)
	)
);

$the_query = new WP_Query( $event_query );

if( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <?php $eventDate = DateTime::createFromFormat('Ymd', $post->event_date)->format('l, F jS'); ?>
    <li><?= $eventDate; ?> - <a href="<?= the_permalink(); ?>"><?= the_title(); ?></a></li>
<?php
endwhile;
endif; 
?>

<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
</ul>	
	
<?php

get_footer();

?>