</div>
<div class="column">
    <?php get_template_part('sidebar', get_post_format()); ?>
</div>
</div>
</div> <!-- /.content -->

</div> <!-- /.container -->


<footer class="footer section">
    <div class="container">
        <div class="content has-text-centered">
            <p> <strong>stamusic.org</strong> is a project by <a href="https://epatr.com">Eric Patrick</a></p>
            
            <div>
                <a href="https://gitlab.com/epatr/wp-list-of-shows"><i class="fa fa-gitlab"></i></a>
            </div>
            
        </div> <!-- /.content.has-text-centered -->
            
    </div> <!-- /.container -->
    
</footer>

<!-- wp_footer() -->
    <?php wp_footer(); ?>
<!-- /wp_footer() -->
</body>
</html>